import { connect } from 'react-redux';
import { loadCampaign } from '../actions';
import Campaign from '../components/Campaign';

const mapStateToProps = (state) => {
    return {
      isLoadingCampaignPending: state.campaign.isLoadingCampaignPending,
      isLoadingCampaignSuccess: state.campaign.isLoadingCampaignSuccess,
      loadingCampaignError: state.campaign.loadingCampaignError,
      campaign: state.campaign.campaign,
      campaigns: state.campaigns.campaigns,
      selectedCampaignIndex: state.campaigns.selectedCampaignIndex,
      iduser:state.loginForm.iduser
    };
  }

const mapDispatchToProps = (dispatch) => {
    return {
      loadCampaign: (campaign) => dispatch(loadCampaign(campaign)) 
    };
  }
  
export default connect(mapStateToProps, mapDispatchToProps)(Campaign)