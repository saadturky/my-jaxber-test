import { connect } from 'react-redux';
import { login } from '../actions';
import LoginForm from '../components/LoginForm';

const mapStateToProps = (state) => {
    return {
      isLoginPending: state.loginForm.isLoginPending,
      isLoginSuccess: state.loginForm.isLoginSuccess,
      loginError: state.loginForm.loginError
    };
  }

const mapDispatchToProps = (dispatch) => {
    return {
        login: (username, password) => dispatch(login(username, password)) 
    };
  }
  
export default connect(mapStateToProps, mapDispatchToProps)(LoginForm)