import React, { Component } from 'react';
import './App.css';
import CreateLoginForm from './containers/CreateLoginForm'
import CreateCampaigns from './containers/CreateCampaigns';
import CreateCampaign from './containers/CreateCampaign';
import CreateAddCampaignItem from './containers/CreateAddCampaignItem';
import CreateRegisterForm from './containers/CreateRegisterForm';

import { HashRouter, Route, Switch } from 'react-router-dom';

class App extends Component {
  render() {
    return (
        <HashRouter>
        <Switch>
          <Route exact path="/" component={CreateLoginForm} />
          <Route path="/campaigns" component={CreateCampaigns} />
          <Route path="/campaign" component={CreateCampaign} />
          <Route path="/addcampaignitem" component={CreateAddCampaignItem} />
          <Route path="/register" component={CreateRegisterForm} />
        </Switch>
        </HashRouter>
    );
  }
}

export default App;
