import { 
  SET_LOADING_CAMPAIGN_PENDING, 
  SET_LOADING_CAMPAIGN_SUCCESS, 
  SET_LOADING_CAMPAIGN_ERROR,
  SET_ADD_CAMPAIGN_ITEM_PENDING,
  SET_ADD_CAMPAIGN_ITEM_SUCCESS,
  SET_ADD_CAMPAIGN_ITEM_ERROR
} from '../actions/types';

export default function campaignReducer(
    state = {
      isLoadingCampaignSuccess: false,
      isLoadingCampaignPending: false,
      loadingCampaignError: null,
      isAddCampaignItemSuccess: false,
      isAddCampaignItemPending: false,
      addCampaignItemError: null,
      campaign: []
  }, action) {
    switch (action.type) {
      case SET_LOADING_CAMPAIGN_PENDING:
        return Object.assign({}, state, {
          isLoadingCampaignPending: action.isLoadingCampaignPending,
          isLoadingCampaignSuccess: false,
          loadingCampaignError: null
        });
  
      case SET_LOADING_CAMPAIGN_SUCCESS:
        return Object.assign({}, state, {
          isLoadingCampaignPending: false,
          isLoadingCampaignSuccess: action.isLoadingCampaingSuccess,
          loadingCampaignError: null,
          campaign: action.campaign
        });
  
      case SET_LOADING_CAMPAIGN_ERROR:
        return Object.assign({}, state, {
          loadingCampaignError: action.loadingCampaignError,
          isLoadingCampaignPending: false,
          isLoadingCampaignSuccess: false
        });

      case SET_ADD_CAMPAIGN_ITEM_PENDING:
        return Object.assign({}, state, {
          isAddCampaignItemPending: action.isAddCampaignItemPending,
          isAddCampaignItemSuccess: false,
          addCampaignItemError: null
        });
  
      case SET_ADD_CAMPAIGN_ITEM_SUCCESS:
        return {
          ...state,
          campaign: [action.campaignItem, ...state.campaign],
          isAddCampaignItemSuccess: true,
          isAddCampaignItemPending: false,
          addCampaignItemError: null
        }
  
      case SET_ADD_CAMPAIGN_ITEM_ERROR:
        return Object.assign({}, state, {
          addCampaignItemError: action.addCampaignItemError,
          isAddCampaignItemPending: false,
          isAddCampaignItemSuccess: false
        });

      default:
        return state;
    }
  }
