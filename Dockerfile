FROM node:alpine
WORKDIR "jaxber/src/App" # ASK JOUNI ABOUT THIS DIRECTORY

COPY package.json .
RUN npm install
COPY . .


# Install Python, Robot framework and other needed packages
RUN apk add bash wget openssl ca-certificates python3 xvfb dbus firefox-esr chromium chromium-chromedriver && \
    python3 --version && \
    pip3 install --upgrade pip && \
    pip3 install robotframework robotframework-seleniumlibrary selenium robotframework-xvfb

CMD ["npm", "start"] # RUN THE APPLICATION
  
# ADD run.sh /usr/local/bin/run.sh
# RUN chmod +x /usr/local/bin/run.sh

# CMD ["run.sh"]