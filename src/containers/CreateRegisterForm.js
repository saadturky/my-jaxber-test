import { connect } from 'react-redux';
import { register } from '../actions';
import RegisterForm from '../components/RegisterForm';

const mapStateToProps = (state) => {
    return {
      isRegisterPending: state.registerForm.isRegisterPending,
      isRegisterSuccess: state.registerForm.isRegisterSuccess,
      registerError: state.registerForm.registerError
    };
  }

const mapDispatchToProps = (dispatch) => {
    return {
        register: (username, password) => dispatch(register(username, password)) 
    };
  }
  
export default connect(mapStateToProps, mapDispatchToProps)(RegisterForm)