import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import red from '@material-ui/core/colors/red';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import ArrowLeftIcon from '@material-ui/icons/ArrowLeft';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
//import List from '@material-ui/core/List';
//import ListItem from '@material-ui/core/ListItem';
//import ListItemIcon from '@material-ui/core/ListItemIcon';
//import TakePictureIcon from '@material-ui/icons/CameraAlt';
//mport SelectPictureIcon from '@material-ui/icons/CameraRoll';
//import RecordVideoIcon from '@material-ui/icons/Videocam';
//import SelectVideoIcon from '@material-ui/icons/VideoLibrary';
import { CardMedia, CardContent } from '@material-ui/core';
import ReactPlayer from 'react-player'

import axios from 'axios';

class AddCampaignItem extends Component {

  constructor(props) {
      super(props);

      this.state = {
          itemtopic: '',
          itemtext: '',
          //dialogOpen: false,
          imageURL: null,
          videoURL: null,
          image: null,
          video: null,
          file: null,
          alertOpen: false,
          alertMessage: '',
          isUploading: false,
          uploadingMessage: ''
      };

      this.updateItemtopicValue = this.updateItemtopicValue.bind(this);
      this.updateItemtextValue = this.updateItemtextValue.bind(this);
      this.showOpenFileDlg = this.showOpenFileDlg.bind(this);
      this.onChangeFile = this.onChangeFile.bind(this);
  }

  updateItemtopicValue(e) {
    this.setState({ itemtopic: e.target.value });
  }  

  updateItemtextValue(e) {
    this.setState({ itemtext: e.target.value });
  } 
  
  
/*
  takePicture = () => {
    this.closeDialog();
    this.props.history.push("/TakePicture");
  }

  selectPicture = () => {
    // disabled
  }

  recordVideo = () => {
    // disabled
  }

  selectVideo = () => {
    // disabled
  }

  addMedia = () => {
    this.setState({dialogOpen: true});
  }

  closeDialog = () => {
    this.setState({dialogOpen: false});
  }
  */

  showOpenFileDlg = () => {
    this.refs.fileUploader.click();
  }

  onChangeFile = (event) => {
    if (event.target.files && event.target.files[0]) {
      let file = event.target.files[0];
      if (file.size / (1024*1024) > 100) {
        this.setState({ alertOpen: true, alertMessage: 'File size is too much (over 100MB). Media is not added, please take a new smaller one.' });
        return;
      }
      this.setState({ file })

      if (file.type.startsWith('image/')) {
       this.setState({ imageURL : URL.createObjectURL(file), videoURL: null});
       }
       if (file.type.startsWith('video/')) {
        this.setState({ videoURL : URL.createObjectURL(file), imageURL: null});
      }
    }
  }

  addCampaignItem = () => {

    this.setState({isUploading: true, uploadingMessage:'Uploading, please wait...'});

    var url = 'https://student.labranet.jamk.fi/~mapas/pwa/jaxber/data/addCampaignItem.php';
    
    const moment = require('moment');
    let date = new Date();
    let created = moment(date.toISOString()).format('YYYY/MM/DD');
    
    var formData = new FormData();

    var iduser = this.props.location.state.iduser;
    var idcampaigns = this.props.location.state.idcampaigns;

    console.log("**idcampaigns=" + idcampaigns);
    
    var filetype = '';
    if (this.state.file !== null) {
      if (this.state.file.type.startsWith('image/')) filetype = "images";
      else filetype = "videos";
    }

    formData.append('file', this.state.file);

    formData.set('iduser',iduser);
    formData.set('idcampaigns',idcampaigns);
    formData.set('created',created);
    formData.set('header',this.state.itemtopic);
    formData.set('text',this.state.itemtext);
    formData.set('filetype',filetype);

    var self = this;
    axios.post(url,
      formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        onUploadProgress: progressEvent => {
          let percentCompleted = Math.floor((progressEvent.loaded * 100) / progressEvent.total);
          // console.log(percentCompleted);
          this.setState({isUploading: true, uploadingMessage:'Uploading, please wait... ('+percentCompleted+' %)'});
        }
      }
      ).then(function (response) {
        console.log(response.data);
        self.setState({ alertOpen: true, alertMessage: response.data.message, isUploading: false, uploadingMessage:'' });
        //if (response.data.status === "error") {
        //  alert(response.data.message);
        //}
      })
      .catch(function (error) {
        //console.log(error);
        self.setState({ alertOpen: true, alertMessage: error, isUploading: false, uploadingMessage:'' });
      });
    }

    handleAlertClose = () => {
      this.setState({ alertOpen: false });
    }

    back = () => {
        this.props.history.goBack();
    }

    render() {
        const { classes } = this.props;
        let { 
            isAddCampaignSuccess,
           // campaigns
        } = this.props;

        if (isAddCampaignSuccess) {
          //console.log("campaigns");
          //console.log(campaigns);
        } else {
          //console.log("isAddCampaingSuccess is false");
          //console.log(campaigns);
        }

        /*
<CardMedia image={this.state.imageURL} className={classes.image}/>
                    <img src={this.state.image} className={classes.image}/>

        */

        return (
            <main className={classes.main}>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton className={classes.menuButton} color="inherit" onClick={_ => this.back()}>
                            <ArrowLeftIcon />
                        </IconButton>
                        <Typography variant="overline" color="inherit" className={classes.grow} noWrap={true}>
                            Jaxber - Add a new Campaign item *
                        </Typography><br/>
                    </Toolbar>
                </AppBar>

                <div className={classes.mydiv}>
                <FormControl margin="normal" required fullWidth>
                    <InputLabel htmlFor="itemtopic" >Topic</InputLabel>
                        <Input id="itemtopic" name="itemtopic" onChange={this.updateItemtopicValue} value={this.state.itemtopic} />
                </FormControl>
                <FormControl margin="normal" required fullWidth>
                    <InputLabel htmlFor="itemtext" >Text</InputLabel>
                        <Input rows={5} multiline={true} id="itemtext" name="itemtext" onChange={this.updateItemtextValue} value={this.state.itemtext} />
                </FormControl>

                {this.state.imageURL !== null &&
                    <CardContent>
                    <CardMedia
                        className={classes.image}
                        image={this.state.imageURL}
                    />
                    </CardContent>
                }
                {this.state.videoURL !== null &&
                    <CardContent>
                    <div className='classes.video'>
                    <ReactPlayer 
                        playsinline
                        controls
                        width='100%'
                        height='100%'
                        url={this.state.videoURL} />
                    </div>
                    </CardContent>
                }

                <input 
                  accept="image/*, video/*"
                  onChange={this.onChangeFile.bind(this)}
                  ref="fileUploader"
                  type="file" id="file" name="file" className="file" style={{display:'none'}}></input>
                <Button 
                  fullWidth  variant="contained" color="primary" className={classes.button} 
                  onClick={_ => this.showOpenFileDlg()}
                 >Add media</Button>
                <Button fullWidth variant="contained" color="primary" className={classes.button} onClick={_ => this.addCampaignItem()} 
                    disabled={this.state.itemtext === '' || this.state.itemtopic === ''}>
                  Add
                </Button>
                <div>
                { this.state.isUploading && <p>{this.state.uploadingMessage}</p> }
                </div>
                </div>

              

              <Dialog
                open={this.state.alertOpen}
                onClose={this.handleAlertClose}
              >
                <DialogTitle>Note</DialogTitle>
                <DialogContent>
                  <DialogContentText>
                    {this.state.alertMessage}
                  </DialogContentText>
                </DialogContent>
                <DialogActions>
                  <Button onClick={this.handleAlertClose} color="primary" autoFocus>
                    Close
                  </Button>
                </DialogActions>
              </Dialog>

            </main>
        )
    }
}

const styles = theme => ({
  mydiv: {
    paddingLeft: '2px',
    paddingRight: '2px'
  },
  videoDiv: {
    position: 'relative',
    paddingTop: '56.25%'
  },
  video: {
      position: 'absolute',
      top: 0,
      left: 0    
  },
  image: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  button: {
      marginTop: theme.spacing.unit
    },
    grow: {
        flexGrow: 1,
      },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    main: {
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        //marginLeft: theme.spacing.unit * 3,
        //marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
          width: 400,
          marginLeft: 'auto',
          marginRight: 'auto',
        },
      },
    card: {
      maxWidth: 400,
      marginTop:5
    },
    media: {
      height: 0,
      paddingTop: '56.25%', // 16:9
    },
    actions: {
      display: 'flex',
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
    lockIcon: {
      float:'right'
    },
    avatar: {
        backgroundColor: red[500],
      },
  });

  AddCampaignItem.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  

export default withStyles(styles)(AddCampaignItem);

/*

<Dialog onClose={this.closeDialog} open={this.state.dialogOpen}>
                <DialogTitle id="simple-dialog-title">Select a media</DialogTitle>
                <div>
                  <List>
                    <ListItem button onClick={_ => this.takePicture()}>
                      <ListItemIcon>
                        <TakePictureIcon />
                      </ListItemIcon>
                      <Typography>Take a picture</Typography>
                    </ListItem>
                    <ListItem button onClick={_ => this.selectPicture()} disabled>
                    <ListItemIcon>
                        <SelectPictureIcon />
                      </ListItemIcon>
                      <Typography>Select a picture</Typography>
                    </ListItem>
                    <ListItem button onClick={_ => this.recordVideo()} disabled>
                    <ListItemIcon>
                        <RecordVideoIcon />
                      </ListItemIcon>
                      <Typography>Record a video</Typography>
                    </ListItem>
                    <ListItem button onClick={_ => this.selectVideo()} disabled>
                    <ListItemIcon>
                        <SelectVideoIcon />
                      </ListItemIcon>
                      <Typography>Select a video</Typography>
                    </ListItem>
                    <ListItem button><Button fullWidth variant="contained" color="primary" onClick={_ => this.closeDialog()} >Cancel</Button></ListItem>
                  </List>
                </div>
              </Dialog>

              */
