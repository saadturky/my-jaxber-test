import { connect } from 'react-redux';
import { addCampaignItem } from '../actions';
import AddCampaignItem from '../components/AddCampaignItem';

const mapStateToProps = (state) => {
    return {
      isAddCampaignSuccess: state.campaigns.isAddCampaignSuccess,
      campaign: state.campaigns.campaign
    };
  }

const mapDispatchToProps = (dispatch) => {
    return {
      addCampaignItem: (campaignItem) => dispatch(addCampaignItem(campaignItem)) 
    };
  }
  
export default connect(mapStateToProps, mapDispatchToProps)(AddCampaignItem)