import { connect } from 'react-redux';
import { loadCampaigns, setSelectedCampaignIndex, addCampaign, showAddCampaignDialog } from '../actions';
import Campaigns from '../components/Campaigns';

const mapStateToProps = (state) => {
    return {
      isLoadingCampaignsPending: state.campaigns.isLoadingCampaignsPending,
      isLoadingCampaignsSuccess: state.campaigns.isLoadingCampaignsSuccess,
      loadingCampaignsError: state.campaigns.loadingCampaignsError,
      campaigns: state.campaigns.campaigns,
      isAddCampaignPending: state.campaigns.isAddCampaignPending,
      isAddCampaignSuccess: state.campaigns.isAddCampaignSuccess,
      addCampaignError: state.campaigns.addCampaignError,
      isAddCampaignsDialogOpen: state.campaigns.isAddCampaignsDialogOpen,
      username: state.loginForm.username,
      iduser: state.loginForm.iduser
    };
  }

const mapDispatchToProps = (dispatch) => {
    return {
      loadCampaigns: () => dispatch(loadCampaigns()),
      setSelectedCampaignIndex: (index) => dispatch(setSelectedCampaignIndex(index)), 
      addCampaign: (campaign) => dispatch(addCampaign(campaign)),  
      showAddCampaignDialog: (isOpen) => dispatch(showAddCampaignDialog(isOpen))  
    };
  }
  
export default connect(mapStateToProps, mapDispatchToProps)(Campaigns)