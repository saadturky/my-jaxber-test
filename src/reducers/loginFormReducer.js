import { SET_LOGIN_ERROR, SET_LOGIN_PENDING, SET_LOGIN_SUCCESS } from '../actions/types';

export default function loginFormReducer(
  state = {
    isLoginSuccess: false,
    isLoginPending: false,
    loginError: null, 
    username: null,
    iduser: null
}, action) {
  switch (action.type) {
    case SET_LOGIN_PENDING:
      return Object.assign({}, state, {
        isLoginPending: action.isLoginPending,
        isLoginSuccess: false,
        loginError: null
      });

    case SET_LOGIN_SUCCESS:
      return Object.assign({}, state, {
        isLoginSuccess: action.isLoginSuccess,
        isLoginPending: false,
        loginError: null,
        username: action.username,
        iduser: action.iduser
      });

    case SET_LOGIN_ERROR:
      return Object.assign({}, state, {
        loginError: action.loginError
      });

    default:
      return state;
  }
}