import { 
  SET_LOGIN_PENDING, 
  SET_LOGIN_SUCCESS, 
  SET_LOGIN_ERROR, 
  SET_LOADING_CAMPAIGNS_PENDING, 
  SET_LOADING_CAMPAIGNS_SUCCESS, 
  SET_LOADING_CAMPAIGNS_ERROR,
  SET_LOADING_CAMPAIGN_PENDING, 
  SET_LOADING_CAMPAIGN_SUCCESS, 
  SET_LOADING_CAMPAIGN_ERROR,
  SET_SELECTED_CAMPAIGN_INDEX,
  SET_ADD_CAMPAIGN_PENDING,
  SET_ADD_CAMPAIGN_ERROR,
  SET_ADD_CAMPAIGN_SUCCESS,
  SET_SHOW_DIALOG,
  SET_ADD_CAMPAIGN_ITEM_PENDING,
  SET_ADD_CAMPAIGN_ITEM_ERROR,
  SET_ADD_CAMPAIGN_ITEM_SUCCESS,
  SET_REGISTER_PENDING, 
  SET_REGISTER_SUCCESS, 
  SET_REGISTER_ERROR 
} from './types';
import axios from 'axios';

/* ADD CAMPAIGN ITEM */
export function addCampaignItem(campaignItem) {
  return dispatch => {
    dispatch(setAddCampaignItemPending(true));
    callAddCampaignItemApi(error => {
      dispatch(setAddCampaignItemPending(false));
      if (!error) {
        dispatch(setAddCampaignItemSuccess(true, campaignItem));
      } else {
        dispatch(setAddCampaignItemError(error));
      }
    });
  }
}

function setAddCampaignItemSuccess(isAddCampaingItemSuccess, campaignItem) {
  return {
    type: SET_ADD_CAMPAIGN_ITEM_SUCCESS,
    isAddCampaingItemSuccess,
    campaignItem
  };
}

function setAddCampaignItemPending(isAddCampaignItemPending) {
  return {
    type: SET_ADD_CAMPAIGN_ITEM_PENDING,
    isAddCampaignItemPending
  };
}

function setAddCampaignItemError(addCampaignItemError) {
  return {
    type: SET_ADD_CAMPAIGN_ITEM_ERROR,
    addCampaignItemError
  }
}

function callAddCampaignItemApi(callback) {
  setTimeout(() => {
      return callback(null);
  }, 1000);
}



/* ADD CAMPAIGN */

export function showAddCampaignDialog(isOpen) {
  return dispatch => {
    dispatch(setShowAddCampaignDialog(isOpen));
  }
}

function setShowAddCampaignDialog(isOpen) {
  return {
    type: SET_SHOW_DIALOG,
    isOpen
  };
}

export function addCampaign(campaign) {
  return dispatch => {
    dispatch(setAddCampaignPending(true));
    axios.get('https://student.labranet.jamk.fi/~mapas/pwa/jaxber/data/addCampaign.php'
        +'?created='+campaign.created
        +'&lastActivity='+campaign.lastActivity
        +'&name='+campaign.name
        +'&private='+campaign.private
        +'&iduser='+campaign.iduser        
      )
      .then(function (response) {
        if (response.data.status === "ok") {
          //console.log(response.data.id);
          campaign.idcampaigns = response.data.id;
          dispatch(setAddCampaignSuccess(true, campaign));
        } else if (response.data.status === "error") {
          dispatch(setAddCampaignSuccess(false));
          dispatch(setAddCampaignPending(false));
          dispatch(setAddCampaignError(new Error('Error adding a new Campaign!')));
        }
      })
      .catch(function (error) {
        dispatch(setLoginError(error.message));
      });  
  }
}

function setAddCampaignSuccess(isAddCampaingSuccess, campaign) {
  return {
    type: SET_ADD_CAMPAIGN_SUCCESS,
    isAddCampaingSuccess,
    campaign
  };
}

function setAddCampaignPending(isAddCampaignPending) {
  return {
    type: SET_ADD_CAMPAIGN_PENDING,
    isAddCampaignPending
  };
}

function setAddCampaignError(addCampaignError) {
  return {
    type: SET_ADD_CAMPAIGN_ERROR,
    addCampaignError
  }
}

/* LOAD CAMPAIGN */

export function loadCampaign(campaign) {
  return dispatch => {
    //console.log(campaign.idcampaigns);
    dispatch(setLoadingCampaignPending(true));
    axios.get('https://student.labranet.jamk.fi/~mapas/pwa/jaxber/data/getCampaign.php?idcampaign='+campaign.idcampaigns)
      .then(function (response) {
        //console.log(response.data);
        dispatch(setLoadingCampaignSuccess(true,response.data));
      })
      .catch(function (error) {
        dispatch(setLoadingCampaignError(error.message));
      });  
  }
}

function setLoadingCampaignPending(isLoadingCampaignPending) {
  return {
    type: SET_LOADING_CAMPAIGN_PENDING,
    isLoadingCampaignPending
  };
}

function setLoadingCampaignSuccess(isLoadingCampaingSuccess, campaign) {
  return {
    type: SET_LOADING_CAMPAIGN_SUCCESS,
    isLoadingCampaingSuccess,
    campaign
  };
}

function setLoadingCampaignError(loadingCampaignError) {
  return {
    type: SET_LOADING_CAMPAIGN_ERROR,
    loadingCampaignError
  }
}

/* CAMPAIGNS */

export function setSelectedCampaignIndex(index) {
  return {
    type: SET_SELECTED_CAMPAIGN_INDEX,
    index
  };
}

export function loadCampaigns() {
  return dispatch => {
    dispatch(setLoadingCampaignsPending(true));
    axios.get('https://student.labranet.jamk.fi/~mapas/pwa/jaxber/data/getCampaigns.php')
      .then(function (response) {
        dispatch(setLoadingCampaignsSuccess(true,response.data));
      })
      .catch(function (error) {
        dispatch(setLoadingCampaignsError(error.message));
      });    
  }
}

function setLoadingCampaignsPending(isLoadingCampaignsPending) {
  return {
    type: SET_LOADING_CAMPAIGNS_PENDING,
    isLoadingCampaignsPending
  };
}

function setLoadingCampaignsSuccess(isLoadingCampaingsSuccess, campaigns) {
  return {
    type: SET_LOADING_CAMPAIGNS_SUCCESS,
    isLoadingCampaingsSuccess,
    campaigns
  };
}

function setLoadingCampaignsError(loadingCampaignsError) {
  return {
    type: SET_LOADING_CAMPAIGNS_ERROR,
    loadingCampaignsError
  }
}

/* LOGIN */

export function login(username, password) {
  return dispatch => {
    dispatch(setLoginPending(true));
    axios.get('https://student.labranet.jamk.fi/~mapas/pwa/jaxber/data/login.php?username='+username+"&password="+password)
      .then(function (response) {
        if (response.data.status === "ok") {
          dispatch(setLoginSuccess(true, username, response.data.iduser));
        } else if (response.data.status === "error") {
          dispatch(setLoginSuccess(false));
          dispatch(setLoginPending(false));
          dispatch(setLoginError(new Error('Invalid username and password')));
        }
      })
      .catch(function (error) {
        dispatch(setLoginError(error.message));
      });  
  }
}

function setLoginPending(isLoginPending) {
  return {
    type: SET_LOGIN_PENDING,
    isLoginPending
  };
}

function setLoginSuccess(isLoginSuccess, username, iduser) {
  return {
    type: SET_LOGIN_SUCCESS,
    isLoginSuccess,
    username, 
    iduser
  };
}

function setLoginError(loginError) {
  return {
    type: SET_LOGIN_ERROR,
    loginError
  }
}

/*
function callLoginApi(username, password, callback) {
  console.log(username+ " " + password);
  setTimeout(() => {
    if (username === 'pasi' && password === 'salainen') {
      return callback(null);
    } else {
      return callback(new Error('Invalid username and password'));
    }
  }, 1000);
}
*/

/* REGISTER */

export function register(username, password) {
  return dispatch => {
    dispatch(setRegisterPending(true));
    axios.get('https://student.labranet.jamk.fi/~mapas/pwa/jaxber/data/register.php?username='+username+"&password="+password)
      .then(function (response) {
        dispatch(setRegisterSuccess(true));
        dispatch(setRegisterPending(false));
      })
      .catch(function (error) {
        dispatch(setRegisterError(error.message));
      });  
  }
}

function setRegisterPending(isRegisterPending) {
  return {
    type: SET_REGISTER_PENDING,
    isRegisterPending
  };
}

function setRegisterSuccess(isRegisterSuccess) {
  return {
    type: SET_REGISTER_SUCCESS,
    isRegisterSuccess
  };
}

function setRegisterError(registerError) {
  return {
    type: SET_REGISTER_ERROR,
    registerError
  }
}